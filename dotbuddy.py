#!/usr/bin/env python

 ###########################################################################
 #                                                                         # 
 #  ConfigParser.h Copyright (C) 2008 by J Rumble                          #
 #                                                                         #
 #  j.w.rumble@reading.ac.uk   				                   #
 #                                                                         #
 #                                                                         # 
 #  This file is part of dotbuddy,                                         #
 #                                                                         #
 #  dotbuddy is free software: you can redistribute it and/or modify       #
 #  it under the terms of the GNU General Public License as published by   #
 #  the Free Software Foundation, either version 2 of the License, or      #
 #  (at your option) any later version.                                    #
 #                                                                         #
 #                                                                         #
 #  dotbuddy is distributed in the hope that it will be useful,            #
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
 #  GNU General Public License for more details.                           #
 #                                                                         #
 #  You should have received a copy of the GNU General Public License      #
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.  #
 ###########################################################################

__version__='v0.1'

import os, sys
import tarfile
import zipfile
import ConfigParser
import time
import md5

#Settings

settings = {
    'archive_type' : 'bz2',
    'confdir' : '.dotbuddy/',
    'confile' : 'dotbuddy.conf',
    'create_archive' : True,
    'create_audit_report' : False,
    'filelist' : 'filelist',
    'logfile' : None,
    'override_perms' : False,
    'silent_override' : True,
    'verbose' : True,
    
}

file_list_names = []

def create_conf_files():
    """
       If a config file is not supplied, or we can't find one at the default
       location, this will attempt to create one using the default config 
       values
    """
    cparser = ConfigParser.ConfigParser()
    npath = os.path.join(os.path.expanduser('~'),settings['confdir'])

    if os.access(npath, os.F_OK) is not True:
        try:
            #Create Path and config file...
            os.mkdir(npath)
            file = open(os.path.join(npath,'dotbuddy.conf'),'w')
        except IOError:
            print "Can't create config file !!"
            sys.exit()
    else:
        cparser.readfp(file,os.path.join(npath,'dotbuddy.conf'))
        cparser.add_section('Options')

    for option, value in settings.iteritems():
        cparser.set('Options',option,value)

    cparser.write(file)
    file.close()
    print cparser.items('Options') 
def main():
    """ 
    Our main Method
    """

    #Find users homedir
    user_home_path = os.path.expanduser("~")

    os.chdir(user_home_path)
    if os.access(settings['confdir'],os.R_OK) is False:
        try:
            pass
            create_conf_files()
        except OSError:
            print "Can't create files.. Exiting !!"
            sys.exit(1) 
    else: 
        create_filelist()
def read_conf():
    """
    Reads Options from config file, and creates one with defaults if it
    doesnt exist.
    """
    session_cfg = ConfigParser.ConfigParser()
    session_cfg = cfg.read('dotbuddy.conf')

def create_filelist():
    infile = open(os.path.join(settings['confdir'],settings['filelist']))
    for line in infile:
        if line.startswith('#'): 
            continue
        elif line.startswith('\n'):
            continue
        else:
             file_list_names.append(line.rstrip())

    if settings['create_audit_report'] is True:
        audit_files()
    if settings['create_archive'] is True:
        archive_files()

def archive_files():
    #if zip is True:
    #    zip_files()
    #elif gzip is True:
    #    gzip_files()
    #else:
    bzip2_files()

def zip_files():
    pass

def gzip_files():
    pass

def bzip2_files():
    date = time.strftime("%d-%b-%y",time.localtime())
    arc_name = "HOME-Backup-" + date + ".tar.bz2" 
    archive = tarfile.open(arc_name,"w:bz2")
    try:
        for tfile in file_list_names:
            try:
                print "Backing up: %s.." % tfile
                archive.add(tfile)
            except OSError:
                print "Error backing up file, skipping.."
                continue
            print  "Finished..\n"

    finally: archive.close()

def audit_files():
    for tfile in file_list_names:
        fdata = open(tfile,'r').read()
        md5sig = md5.md5(fdata).hexdigest()
        print "Name: %s MD5 Sig: %s" % (tfile,md5sig)



if __name__ == '__main__':
    main()

